# ArduinoLabView Controler

## Opis 
Prosty kontroler umożliwiający pobieranie pomiarów z czujników z wykorzystaniem arduino oraz sterowanie portami za pomocą komputera.
## Konfiguracja
Czujniki:
- DHT-11 - D11
- fotorezystor - A3
- LPS331 - A5 -> SCL, A4 -> SDS

Wyświetlacz HD44780:
- D10 -> RS 
- D9 -> RW
- D8 -> E
- D7 -> d4
- D6 -> d5
- D5 -> d6
- D4 -> d7

Przycisk zmieniający menu (przerwanie na zboczu narastającym):
- D2 (zewnętrzny PULL-DOWN)

Sterowalne piny GPIO:
- A0
- A1

## Komunikacja
Przebiega po porcie szeregowym z przędkością 115200.
### Polecenia:
'M' zwraca aktualne wartości z czujników odzielone spacjami:
- temperatura (0-50) w stopniach Celsjusza
- wilgotność (0-100) w procentach
- ciśnienie w hPa
- jasność (0.0-1.0)

'F' ustawia pin A0 w stan wysoki 

'f' ustawia pin A0 w stan niski 

'S' ustawia pin A1 w stan wysoki 

's' ustawia pin A0 w stan niski

