#include <LiquidCrystal.h>
#include <Wire.h>

#include "dht.h"
#include "LPS331.h"

#define RSS_SPEED 115200
#define DHT11_PIN 11
#define LIGHT_ANALOG 3
// PIN 2
#define BUTTON_INTERRUPT 0

#define FIRST_DIGITAL_OUT A0
#define SECOND_DIGITAL_OUT A1

// hardware
LiquidCrystal lcd(10, 9, 8, 7, 6, 5, 4);
dht DHT;
LPS331 pressureSensor;

// results
float temperature;
float humidity;
float pressure;
float lightLevel;

//some variables
byte view;
unsigned counter;

void setup() {
  Serial.begin(RSS_SPEED);
  lcd.begin(16, 2);
  Wire.begin();
  pressureSensor.init();
  pressureSensor.enableDefault();
  attachInterrupt(BUTTON_INTERRUPT, displayNextMenu, RISING);
  pinMode(FIRST_DIGITAL_OUT,OUTPUT);
  pinMode(SECOND_DIGITAL_OUT,OUTPUT);
  view=0;
}

void serialEvent(){
  while(Serial.available()){
    char command=Serial.read();
    switch(command){

    case 'M':
      printResults();  
      break;

    case 'F':
      digitalWrite(FIRST_DIGITAL_OUT,HIGH);
      updateDisplay();
      break;

    case 'f':
      digitalWrite(FIRST_DIGITAL_OUT,LOW);
      updateDisplay();
      break;

    case 'S':
      digitalWrite(SECOND_DIGITAL_OUT,HIGH);
      updateDisplay();
      break;

    case 's':
      digitalWrite(SECOND_DIGITAL_OUT,LOW);
      updateDisplay();
      break;
    }
  } 
}

void collectDataFromSensors(){
  DHT.read11(DHT11_PIN);
  humidity=DHT.humidity;
  temperature=DHT.temperature;
  pressure=pressureSensor.readPressureMillibars();
  lightLevel=analogRead(LIGHT_ANALOG)/1023.0;

}

void displayNextMenu(){
  view=(view+1)%3;
  updateDisplay();
}

void updateDisplay(){
  lcd.clear();

  switch (view){
  case 0:
    lcd.setCursor(0, 0);
    lcd.print("Temp: ");
    lcd.print(temperature);

    lcd.setCursor(0, 1);
    lcd.print("Humidity: ");
    lcd.print(humidity);
    break;

  case 1:
    lcd.setCursor(0, 0);
    lcd.print("Pressure: ");
    lcd.print(pressure);

    lcd.setCursor(0, 1);
    lcd.print("Light: ");
    lcd.print(lightLevel);
    break;
  case 2:
    lcd.setCursor(0, 0);
    lcd.print("GPIO_1: ");
    lcd.print(digitalRead(FIRST_DIGITAL_OUT)?" ON ":" OFF ");

    lcd.setCursor(0, 1);
    lcd.print("GPIO_2: ");
    lcd.print(digitalRead(SECOND_DIGITAL_OUT)?" ON ":" OFF ");
    
  }

}
void printResults(){
  Serial.print(temperature);
  Serial.print(" ");
  Serial.print(humidity);
  Serial.print(" ");
  Serial.print(pressure); 
  Serial.print(" ");
  Serial.print(lightLevel);
  Serial.println(" ");

}

void loop() {
  if(counter%500==0){
      collectDataFromSensors(); 
      updateDisplay();
   
  }
  delay(10);
  counter++;
}



